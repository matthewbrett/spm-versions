# SPM archives

These are my personal archives of some SPM downloads.

They are in some completely arbitrary state, I store them here only for my own
convenience.

Please don't use these downloads to get a real copy of SPM. Instead, please go
to the [SPM home page](http://www.fil.ion.ucl.ac.uk/spm) where you will find
the canonical download sources.

Matthew Brett
